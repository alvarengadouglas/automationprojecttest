package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"src/test/java/Features"}
		,glue= {"stepdefinitions"}
		,tags = "@End2End"
		)
public class TesteRunner {

}
