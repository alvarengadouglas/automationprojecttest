package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import stepdefinitions.Hooks;

public class MetodosUteis {
	
	protected WebDriver driver;
	
	public void esperarElemento(WebElement elemento) {
		driver = Hooks.getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(elemento));
	}
	
	public void selecionar(WebElement elemento, String texto) {
		driver = Hooks.getDriver();
		Select dropDown = new Select(elemento);
		dropDown.selectByVisibleText(texto);
	}

	//Os elementos do header foram separados na super classe para facilitar a o acesso nas demais classes e diminuir a escrita de codigo
	@FindBy(how = How.ID, using = "react-burger-menu-btn")
	protected WebElement hamburgerMenu;

	@FindBy(how= How.ID, using = "shopping_cart_container")
	protected WebElement cartContainer;

	@FindBy(how= How.ID, using = "logout_sidebar_link")
	protected WebElement logoutOption;

	public void clickHamburgerMenu(){
		esperarElemento(hamburgerMenu);
		hamburgerMenu.click();
	}
	public void clickLogoutOption(){
		esperarElemento(logoutOption);
		logoutOption.click();
	}

	public void aguardar() throws InterruptedException {
		Thread.sleep(1000);
	}



	
}
