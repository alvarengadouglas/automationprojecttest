package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import utils.MetodosUteis;

import static org.junit.Assert.*;

public class SwagLoginPage extends MetodosUteis {

    protected WebDriver driver;
    private String inventoryUrl;
    private String expectedInventoryUrl = "https://www.saucedemo.com/inventory.html";

    public SwagLoginPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver , this);
    }

    @FindBy(how = How.ID, using = "user-name")
    private WebElement inputUserName;

    @FindBy(how = How.ID, using = "password")
    private WebElement inputPassord;

    @FindBy(how = How.ID, using = "login-button")
    private WebElement btnLogin;

    public void preencherUserName(String username) throws InterruptedException{
        esperarElemento(inputUserName);
        inputUserName.sendKeys(username);
        aguardar();
    }

    public void preencherPassword(String password) throws InterruptedException{
        esperarElemento(inputPassord);
        inputPassord.sendKeys(password);
        aguardar();
    }

    public void btnLoginClick() throws InterruptedException {
        esperarElemento(btnLogin);
        btnLogin.click();
        aguardar();
    }

    public void validarInventoryPage(){
        esperarElemento(hamburgerMenu);
        inventoryUrl = driver.getCurrentUrl();
        assertEquals(expectedInventoryUrl, inventoryUrl);
        assertTrue(hamburgerMenu.isEnabled());
        assertTrue(cartContainer.isEnabled());
    }




}
