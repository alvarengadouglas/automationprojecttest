package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import utils.MetodosUteis;

import static java.lang.Integer.parseInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class InventoryPage extends MetodosUteis {

    WebDriver driver;
    private int qtdProdutos;
    String urlPage = "https://www.saucedemo.com/inventory.html";
    private String expectedLoginPageUrl = "https://www.saucedemo.com/";
    private String loginPageUrl;
    private String expectedUrlCart = "https://www.saucedemo.com/cart.html";

    public InventoryPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "add-to-cart-sauce-labs-backpack")
    private WebElement addBackpackButton;

    @FindBy(how = How.ID, using = "add-to-cart-sauce-labs-bike-light")
    private WebElement addBikeLightButton;

    @FindBy(how = How.ID, using = "add-to-cart-sauce-labs-bolt-t-shirt")
    private WebElement addBoltTShirtButton;

    @FindBy(how = How.ID, using = "add-to-cart-sauce-labs-fleece-jacket")
    private WebElement addFleeceJacketButton;

    @FindBy(how = How.ID, using = "add-to-cart-sauce-labs-onesie")
    private WebElement addOnesieButton;

    @FindBy(how = How.ID, using = "add-to-cart-test.allthethings()-t-shirt-(red)")
    private WebElement addTShirtRedButton;

    @FindBy(how = How.XPATH, using = "//*[@id='shopping_cart_container']/a/span")
    private WebElement amountItemsCart;

    @FindBy(how = How.ID, using = "shopping_cart_container")
    private WebElement cartButton;

    @FindBy(how = How.ID, using = "title")
    private WebElement titlePage;

    @FindBy(how = How.ID, using = "remove-sauce-labs-backpack")
    private WebElement btnRemoveBackpack;

    @FindBy(how = How.ID, using = "remove-sauce-labs-bike-light")
    private WebElement btnRemoveBikeLight;

    @FindBy(how = How.ID, using = "remove-sauce-labs-bolt-t-shirt")
    private WebElement btnRemoveBoltTshirt;

    @FindBy(how = How.ID, using = "remove-sauce-labs-fleece-jacket")
    private WebElement btnRemoveJacket;

    @FindBy(how = How.ID, using = "remove-sauce-labs-onesie")
    private WebElement btnRemoveOnesie;

    @FindBy(how = How.ID, using = "remove-test.allthethings()-t-shirt-(red)")
    private WebElement btnRemoveTshirtRed;

    @FindBy(how = How.ID, using = "user-name")
    private WebElement inputUserName;

    @FindBy(how = How.ID, using = "password")
    private WebElement inputPassord;

    @FindBy(how = How.ID, using = "login-button")
    private WebElement btnLogin;

    @FindBy(how = How.XPATH, using = "//*[@id='header_container']/div[2]/span")
    private WebElement titleCartPage;

    @FindBy(how = How.ID, using = "checkout")
    private WebElement btnCheckout;

    public void addBackPackToCart() throws InterruptedException {
        esperarElemento(addBackpackButton);
        addBackpackButton.click();
        qtdProdutos += 1;
        aguardar();
    }

    public void addBikeLightToCart() throws InterruptedException {
        esperarElemento(addBikeLightButton);
        addBikeLightButton.click();
        qtdProdutos += 1;
        aguardar();
    }

    public  void addAddBoltTShirtToCart() throws InterruptedException {
        esperarElemento(addBoltTShirtButton);
        addBoltTShirtButton.click();
        qtdProdutos += 1;
        aguardar();
    }

    public void addFleeceJacketToCart() throws InterruptedException {
        esperarElemento(addFleeceJacketButton);
        addFleeceJacketButton.click();
        qtdProdutos += 1;
        aguardar();
    }

    public void addOnesieToCart() throws InterruptedException {
        esperarElemento(addOnesieButton);
        addOnesieButton.click();
        qtdProdutos += 1;
        aguardar();
    }

    public void addTShirtRedToCart() throws InterruptedException {
        esperarElemento(addTShirtRedButton);
        addTShirtRedButton.click();
        qtdProdutos += 1;
        aguardar();
    }

    public void removeBackPackToCart() throws InterruptedException {
        esperarElemento(btnRemoveBackpack);
        btnRemoveBackpack.click();
        qtdProdutos -= 1;
        aguardar();
    }

    public void removeBikeLightToCart() throws InterruptedException {
        esperarElemento(btnRemoveBikeLight);
        btnRemoveBikeLight.click();
        qtdProdutos -= 1;
        aguardar();
    }

    public  void removeAddBoltTShirtToCart() throws InterruptedException {
        esperarElemento(btnRemoveBoltTshirt);
        btnRemoveBoltTshirt.click();
        qtdProdutos -= 1;
        aguardar();
    }

    public void removeFleeceJacketToCart() throws InterruptedException {
        esperarElemento(btnRemoveJacket);
        btnRemoveJacket.click();
        qtdProdutos -= 1;
        aguardar();
    }

    public void removeOnesieToCart() throws InterruptedException {
        esperarElemento(btnRemoveOnesie);
        btnRemoveOnesie.click();
        qtdProdutos -= 1;
        aguardar();
    }

    public void removeTShirtRedToCart() throws InterruptedException {
        esperarElemento(btnRemoveTshirtRed);
        btnRemoveTshirtRed.click();
        qtdProdutos -= 1;
        aguardar();
    }

    public int getAmountItemsCart(){
        return parseInt(this.amountItemsCart.getText());
    }

    public void clickCartButton() throws InterruptedException {
        esperarElemento(cartButton);
        cartButton.click();
        aguardar();
    }

    public void validateAmountItemsCart(){
        assertEquals(getAmountItemsCart(), qtdProdutos);
    }

    public String getTitlePage(){
        return titlePage.getText();
    }

    public void validatePageInventory(){
        esperarElemento(cartButton);
        esperarElemento(hamburgerMenu);
        esperarElemento(titlePage);
        String url = driver.getCurrentUrl();
        assertEquals(urlPage, url);
        assertTrue(cartButton.isEnabled());
        assertTrue(hamburgerMenu.isEnabled());
        assertEquals("PRODUCTS", getTitlePage());
    }

    public void validarLoginPageLogout(){
        esperarElemento(inputUserName);
        esperarElemento(inputPassord);
        esperarElemento(btnLogin);
        loginPageUrl = driver.getCurrentUrl();
        assertTrue(inputUserName.isEnabled());
        assertTrue(inputPassord.isEnabled());
        assertTrue(btnLogin.isEnabled());
        assertEquals(expectedLoginPageUrl, loginPageUrl);
    }

    public void validarCartPage(){
        esperarElemento(btnCheckout);
        esperarElemento(titleCartPage);
        String actualUrl = driver.getCurrentUrl();
        assertEquals(expectedUrlCart, actualUrl);
        assertTrue(btnCheckout.isEnabled());
        assertTrue(titleCartPage.isEnabled());
    }




















}
