#language: pt
@Carrinho @End2End
Funcionalidade: Carrinho

  Contexto:
    Dado que o usuario esteja na pagina principal do sistema
    E que o usuario esteja logado
    E adiciono ao carrinho um produto "<produto>"
    E clico no icone do carrinho


  @CheckoutCarrinho @SmokeTest
  Esquema do Cenario:
    Quando a tela e carregada
    E verifico a quantidade de itens adicionados ao carrinho
    E clico no botao de checkout
    Entao o sistema direciona para a tela de checkout passo 1

    Exemplos:
      ||


