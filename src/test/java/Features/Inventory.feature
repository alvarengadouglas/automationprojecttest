#language: pt
@Inventory @End2End
Funcionalidade: Tela de produtos

  Contexto:
    Dado que o usuario esteja na tela principal do sistema
    E que o usuario esteja logado

  @AdicionarProdutoAoCarrinho @SmokeTest
  Esquema do Cenario:
    Quando adiciono ao carrinho um produto "<produto>"
    E clico no icone do carrinho
    Entao o sistema direciona para a tela de carrinho

    Exemplos:
      ||

  @RealizarLogout @SmokeTest
  Esquema do Cenario:
    Quando clico no menu hamburger
    E clico na opcao logout
    Entao o sistema desconecta o usuario da plataforma e o direciona para tela de login

    Exemplos:
      ||


