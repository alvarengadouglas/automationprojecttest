#language: pt
@Login @End2End
Funcionalidade: Login

  Contexto:
    Dado que o usuario esteja na tela principal do sistema

  @RealizarLogin @SmokeTest
  Esquema do Cenario:
    Quando preencher o campo username como "<user>"
    E preencher o campo de password como "<password>"
    E clicar no botao de login
    Entao o sistema autoriza o login e direciona para a pagina de inventario.

    Exemplos:
    |user|password|
    |standard_user|secret_sauce|

