package stepdefinitions;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Hooks {

	//credenciais browserstack
	public static final String USERNAME = "douglasbarbosa_XvCB0i";
	public static final String AUTOMATE_KEY = "4r58TuissSm3dTafzxmV";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	private static WebDriver driver = null;

	// configs para rodar com driver local

	// public static WebDriver driver;
	// protected final String MAC = "/Users/testdebranol/chromedriver";
	// protected final String WINDOWS = "C://chromedriver.exe";

	@Before
	public void iniciarDriver() {

		// configuracao local driver
		// System.setProperty("webdriver.chrome.driver", MAC);
	    // driver = new ChromeDriver();
	    // driver.manage().window().maximize();
	    // driver.manage().deleteAllCookies();
	    // driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	    // driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	    //configuracao cross browser
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("os_version", "High Sierra");
		caps.setCapability("resolution", "1920x1080");
		caps.setCapability("browser", "Chrome");
		caps.setCapability("browser_version", "83.0");
		caps.setCapability("os", "OS X");

		try {
			//instancia e configuracao do browser
			driver = new RemoteWebDriver(new URL(URL), caps);
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.manage().window().maximize();

		} catch (Exception e) {
			System.out.println("Houve um problema com a URL: " + e.getMessage());
		}
	}
	
	@After
	public void fecharDriver() {
		driver.quit();
	}
	
	public static WebDriver getDriver()
    {
        return driver;
    }

    public static void abrirUrl(String url)
    {
        driver.get(url);
    }

    
}
