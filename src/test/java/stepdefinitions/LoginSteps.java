package stepdefinitions;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.WebDriver;


import pageobjects.OrangeLoginPage;

public class LoginSteps {
	WebDriver driver;
	
	@Dado("^que o usuario esteja na pagina principal do sistema$")
	public void que_o_usuario_esteja_na_pagina_principal_do_sistema() throws Throwable {
		Hooks.abrirUrl("http://opensource-demo.orangehrmlive.com/");
		driver = Hooks.getDriver();
	}
	
	@Dado("^que usuario esteja logado$")
	public void que_usuario_esteja_logado() throws Throwable {
		OrangeLoginPage lp = new OrangeLoginPage(driver);
		lp.preencherUsuario("Admin");
		lp.preencherSenha("admin123");
		lp.clicarBotaoLogin();
	}

	@Quando("^informar o campo Username como \"([^\"]*)\"$")
	public void informar_o_campo_Username_como(String arg1) throws Throwable {
		OrangeLoginPage lp = new OrangeLoginPage(driver);
		lp.preencherUsuario(arg1);
	}

	@Quando("^informar o campo Password como \"([^\"]*)\"$")
	public void informar_o_campo_Password_como(String arg1) throws Throwable {
		OrangeLoginPage lp = new OrangeLoginPage(driver);
		lp.preencherSenha(arg1);
	}

	@Quando("^clicar no botao Login$")
	public void clicar_no_botao_Login() throws Throwable {
		OrangeLoginPage lp = new OrangeLoginPage(driver);
		lp.clicarBotaoLogin();
	}

	@Entao("^o sistema devera autorizar o login, exibindo pagina contendo o Dashboard$")
	public void o_sistema_devera_autorizar_o_login_exibindo_pagina_contendo_o_Dashboard() throws Throwable {
		OrangeLoginPage lp = new OrangeLoginPage(driver);
		lp.validarDashboard();
	}
}
