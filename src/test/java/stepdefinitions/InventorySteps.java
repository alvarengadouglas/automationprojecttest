package stepdefinitions;

import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.WebDriver;
import pageobjects.InventoryPage;

public class InventorySteps {

    WebDriver driver;

    @Quando("^adiciono ao carrinho um produto \"([^\"]*)\"$")
    public void adiciono_ao_carrinho_um_produto(String arg1) throws Throwable {
        InventoryPage ip = new InventoryPage(Hooks.getDriver());
        ip.validatePageInventory();
        ip.addBackPackToCart();
        ip.validateAmountItemsCart();
    }

    @Quando("^clico no icone do carrinho$")
    public void clico_no_icone_do_carrinho() throws Throwable {
        InventoryPage ip = new InventoryPage(Hooks.getDriver());
        ip.clickCartButton();
    }

    @Entao("^o sistema direciona para a tela de carrinho$")
    public void o_sistema_direciona_para_a_tela_de_carrinho() throws Throwable {
        InventoryPage ip = new InventoryPage(Hooks.getDriver());
        ip.validarCartPage();
    }


    @Quando("^clico no menu hamburger$")
    public void clico_no_menu_hamburger() throws Throwable {
        InventoryPage ip = new InventoryPage(Hooks.getDriver());
        ip.clickHamburgerMenu();
    }

    @Quando("^clico na opcao logout$")
    public void clico_na_opcao_logout() throws Throwable {
        InventoryPage ip = new InventoryPage(Hooks.getDriver());
        ip.clickLogoutOption();
    }

    @Entao("^o sistema desconecta o usuario da plataforma e o direciona para tela de login$")
    public void o_sistema_desconecta_o_usuario_da_plataforma_e_o_direciona_para_tela_de_login() throws Throwable {
        InventoryPage ip = new InventoryPage(Hooks.getDriver());
        ip.validarLoginPageLogout();
    }

}