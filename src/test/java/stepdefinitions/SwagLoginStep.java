package stepdefinitions;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.WebDriver;
import pageobjects.SwagLoginPage;

public class SwagLoginStep {

    WebDriver driver;

    @Dado("^que o usuario esteja na tela principal do sistema$")
    public void que_o_usuario_esteja_na_tela_principal_do_sistema() throws Throwable {
        Hooks.abrirUrl("https://www.saucedemo.com/");
        driver = Hooks.getDriver();
    }


    @Dado("^que o usuario esteja logado$")
    public void que_o_usuario_esteja_logado() throws Throwable{
        SwagLoginPage swp = new SwagLoginPage(driver);
        swp.preencherUserName("standard_user");
        swp.preencherPassword("secret_sauce");
        swp.btnLoginClick();
    }

    @Quando("^preencher o campo username como \"([^\"]*)\"$")
    public void preencher_o_campo_username_como(String arg1) throws Throwable {
        SwagLoginPage swagLoginPage = new SwagLoginPage(driver);
        swagLoginPage.preencherUserName(arg1);
    }

    @Quando("^preencher o campo de password como \"([^\"]*)\"$")
    public void preencher_o_campo_de_password_como(String arg1) throws Throwable {
        SwagLoginPage swagLoginPage = new SwagLoginPage(driver);
        swagLoginPage.preencherPassword(arg1);
    }

    @Quando("^clicar no botao de login$")
    public void clicar_no_botao_de_login() throws Throwable {
        SwagLoginPage swagLoginPage = new SwagLoginPage(driver);
        swagLoginPage.btnLoginClick();
    }

    @Entao("^o sistema autoriza o login e direciona para a pagina de inventario\\.$")
    public void o_sistema_autoriza_o_login_e_direciona_para_a_pagina_de_inventario() throws Throwable {
        SwagLoginPage swagLoginPage = new SwagLoginPage(driver);
        swagLoginPage.validarInventoryPage();
    }

}
