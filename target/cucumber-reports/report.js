$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Inventory.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 3,
  "name": "Tela de produtos",
  "description": "",
  "id": "tela-de-produtos",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "line": 2,
      "name": "@Inventory"
    },
    {
      "line": 2,
      "name": "@End2End"
    }
  ]
});
formatter.scenarioOutline({
  "line": 10,
  "name": "",
  "description": "",
  "id": "tela-de-produtos;",
  "type": "scenario_outline",
  "keyword": "Esquema do Cenario",
  "tags": [
    {
      "line": 9,
      "name": "@AdicionarProdutoAoCarrinho"
    },
    {
      "line": 9,
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "adiciono ao carrinho um produto \"\u003cproduto\u003e\"",
  "keyword": "Quando "
});
formatter.step({
  "line": 12,
  "name": "clico no icone do carrinho",
  "keyword": "E "
});
formatter.step({
  "line": 13,
  "name": "o sistema direciona para a tela de carrinho",
  "keyword": "Entao "
});
formatter.examples({
  "line": 15,
  "name": "",
  "description": "",
  "id": "tela-de-produtos;;",
  "rows": [
    {
      "cells": [
        ""
      ],
      "line": 16,
      "id": "tela-de-produtos;;;1"
    }
  ],
  "keyword": "Exemplos"
});
formatter.scenarioOutline({
  "line": 19,
  "name": "",
  "description": "",
  "id": "tela-de-produtos;",
  "type": "scenario_outline",
  "keyword": "Esquema do Cenario",
  "tags": [
    {
      "line": 18,
      "name": "@RealizarLogout"
    },
    {
      "line": 18,
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "line": 20,
  "name": "clico no menu hamburger",
  "keyword": "Quando "
});
formatter.step({
  "line": 21,
  "name": "clico na opcao logout",
  "keyword": "E "
});
formatter.step({
  "line": 22,
  "name": "o sistema desconecta o usuario da plataforma e o direciona para tela de login",
  "keyword": "Entao "
});
formatter.examples({
  "line": 24,
  "name": "",
  "description": "",
  "id": "tela-de-produtos;;",
  "rows": [
    {
      "cells": [
        ""
      ],
      "line": 25,
      "id": "tela-de-produtos;;;1"
    }
  ],
  "keyword": "Exemplos"
});
});